# Building FakeOS

## Dependencies

You need to install the following applications before you can build this:

- **node** v15+
- **npm** v7+
- **yarn** (`npm install -g yarn`) v1.x

---

<details>
  <summary>My dependency versions</summary>
  
  - **node** v16.5.0 (installed from AUR)
  - **npm** v7.20.2 (installed from AUR)
  - **yarn** v1.22.11 
  
</details>

---

Now, you can install the project dependencies:

```shell
$ yarn install
```

If you get `gyp` errors, you may need to install build tools (`apt install -y build-essential` or `pacman -S base-devel`)

## Building assets

Just run the following command:

```shell
$ yarn webpack
```

## Running

**WARNING:** You have to build the assets before doing that.

```shell
$ yarn start
```

## Development Server

The server will start on http://127.0.0.1:9000

```shell
$ yarn dev
```
