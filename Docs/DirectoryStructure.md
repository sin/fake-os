# FakeOS Directory Structure

```shell
FakeOS
├── Docs              # FakeOS Documentation
├── Meta              # Metadata that is not related to FakeOS
│   └── Start.ts      # FakeOS Start Script
├── node_modules      # Node.js Modules (Generated with `yarn install`)
├── package.json      # Node.js Package Information
├── README.md         # README.md
├── Www               # FakeOS Webroot
└── yarn.lock         # Yarn lockfile
```
