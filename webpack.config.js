const path = require("path");

module.exports = {
  entry: "./Kernel/Init.ts",
  devtool: "inline-source-map",
  mode: "development",
  module: {
    rules: [
      {
        test: /\.ts?$/,
        use: "ts-loader",
        exclude: /node_modules/,
      },
    ],
  },
  output: {
    path: path.resolve(__dirname, "WWW"),
    filename: "./bundle.js",
  },
  devServer: {
    contentBase: path.join(__dirname, "WWW"),
    compress: false,
    filename: "./build/bundle.js",
    port: 9000,
  },
  resolve: {
    extensions: [".ts", ".js"],
  },
};
