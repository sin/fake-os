import ServeHandler from "serve-handler";
import HTTP from "http";

const Arguments = process.argv;

if (Arguments.length === 3) {
  if (Arguments[2] == "--help") {
    console.log(
      `${Arguments[1]} [ARGS | <port>]\n\n --help  displays usage of this script`
    );
    process.exitCode = 0;
    process.exit();
  }
}

const Server = HTTP.createServer((req, res) => {
  return ServeHandler(req, res);
});
const Port = Arguments[2] ?? 8080;

Server.listen(Port, () => {
  console.log(
    "[FakeOS] The server is running!",
    `\n[FakeOS] Running on Port ${Port}`,
    `\n[FakeOS] URL: http://127.0.0.1:${Port}/`
  );
});
