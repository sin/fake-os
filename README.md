# FakeOS

FakeOS is an operating system running in the browser. It's not really an operating system, but that's why it is called "FakeOS". It's written in TypeScript and runs completly in the browser.

## Building & running locally

See [Docs/Building.md](./Docs/Building.md)
