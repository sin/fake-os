import Logging from "../Kernel/Logging";
import main from "./Shell/Shell";

export default function Init() {
  Logging.info("[userspace] Hello, world!");
  main();
}
