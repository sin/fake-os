import { GetDoc, GetScreen } from "../../Kernel/Canvas";
import TextConsole from "../../Kernel/Graphics/TextConsole";
import { Shell } from "./Shell";

enum Key {
  ENTER = 13,
  SHIFT = 16,
  CTRL = 17,
  CTRL_RIGHT = 17,
  ALT = 18,
  OS = 91,
  ALT_RIGHT = 225,
}

async function EventListener(event: KeyboardEvent) {
  event.preventDefault();
  GetScreen().scrollIntoView({ block: "end" });

  if (
    [Key.CTRL, Key.ALT, Key.ALT_RIGHT, Key.CTRL_RIGHT, Key.SHIFT].includes(
      event.keyCode
    )
  )
    return;
  if (event.keyCode == Key.ENTER) {
    Shell.executed();
    return;
  }
  Shell.command += event.key;
  TextConsole.write_str(event.key);
}

GetDoc().addEventListener("keydown", EventListener);

export default EventListener;
