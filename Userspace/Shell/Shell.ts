import Logging from "../../Kernel/Logging";
import TextConsole from "../../Kernel/Graphics/TextConsole";
import "./EventListener";
import Package from "../../package.json";
import Command from "./Command";
import Version from "./Commands/version";
import Help from "./Commands/help";

export class Shell {
  private static _command: string = "";
  public static commands: Map<string, Command> = new Map<string, Command>([
    ["help", new Help()],
    ["version", new Version()],
  ]);

  public static executed(): void {
    TextConsole.write_str("\n");
    if (!this.commands.has(this.command.split(" ")[0])) {
      Logging.error("Command not found!");
      this._command = "";
      this.shell();

      return;
    }
    this.commands.get(this.command.split(" ")[0]).exec(this.command.split(" "));
    this._command = "";
    this.shell();
  }

  public static shell(): void {
    TextConsole.write_str("\n (master@fake) % ");
  }

  public static set command(value: string) {
    if (!/[A-Za-z0-9]/.test(value)) {
      throw new TypeError("Invalid value passed to command setter.");
    }
    this._command = value;
  }

  public static get command(): string {
    return this._command;
  }
}

export default function main(): number {
  TextConsole.write_str(
    `\n Welcome to the FakeOS Shell! Running version ${Package.version}`
  );
  Shell.shell();
  return 0;
}
