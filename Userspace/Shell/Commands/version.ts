import TextConsole from "../../../Kernel/Graphics/TextConsole";
import Command from "../Command";

export default class Version extends Command {
  command_name = "version";
  exec(): number {
    TextConsole.write_str(
      `Version: ${require("../../../package.json").version}`
    );
    return 0;
  }
}
