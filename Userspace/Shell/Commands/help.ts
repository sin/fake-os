import TextConsole from "../../../Kernel/Graphics/TextConsole";
import Command from "../Command";
import { Shell } from "../Shell";

export default class Help extends Command {
  command_name = "help";
  exec(): number {
    TextConsole.write_str(`Command list: \n`);
    Shell.commands.forEach((val) => {
      TextConsole.write_str(`${val.command_name} `);
    });
    return 0;
  }
}
