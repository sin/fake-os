export default abstract class Command {
  public abstract command_name: string;
  public abstract exec(args: string[]): number;
}
