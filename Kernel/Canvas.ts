import Logging from "./Logging";

interface Size {
  width: number;
  height: number;
}

class Canvas {
  public static Init(): void {
    GetScreen().style.backgroundColor = "#000";
    GetScreen().style.minHeight = "100vh";
    GetScreen().style.maxWidth = "100vw";
    GetDoc().body.style.margin = "0px";

    Logging.info("Canvas has been initialized");
    Logging.info("Screen size: %ix%ipx", this.size.width, this.size.height);
  }

  public static get size(): Size {
    return {
      width: GetScreen().clientWidth,
      height: GetScreen().clientHeight,
    };
  }
}

function GetScreen(): HTMLElement {
  return document.getElementById("app")!;
}

function GetDoc(): Document {
  return document;
}

export default Canvas;
export { GetScreen, GetDoc };
