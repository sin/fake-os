export const ERROR_NONE = 0;

// File system
export const ERROR_NOTFOUND = 1;
export const ERROR_ISDIR = 2;
export const ERROR_ISFILE = 3;
export const ERROR_NOACCESS = 4;
export const ERROR_EXISTS = 5;
export const ERROR_FAILED = 6;
