import Logging from "./Logging";

class KernelPanic extends Error {
  constructor(message: string) {
    super(message);
    this.name = "KernelPanic";
  }
}

function Panic(message: string) {
  const panic = new KernelPanic(message);
  Logging.error(`
KERNEL PANIC!
There was an internal error and FakeOS has panicked.\n
${panic}
\nStacktrace:
${panic.stack}`);

  window.stop();

  throw panic;
}

function PanicErrorHandler(error: ErrorEvent): void {
  Logging.error(`${error.error}`);
  Panic(`${error.error}`);
}

export default Panic;
export { PanicErrorHandler };
