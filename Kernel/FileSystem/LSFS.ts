import {
  ERROR_EXISTS,
  ERROR_FAILED,
  ERROR_NONE,
  ERROR_NOTFOUND,
} from "../Errors";
import Logging from "../Logging";

export enum WriteFlags {
  CREATE_AND_WRITE = 1,
  WRITE = 2,
}

export default class LocalStorageFileSystem {
  private static _storage = window.localStorage;
  private static get storage(): Storage {
    return this._storage;
  }

  // Helper functions
  static parse(): Array<Record<string, any>> {
    const item = this.storage.getItem("FileSystem");
    if (!item) {
      Logging.debug(
        "Local Storage item 'FileSystem' is undefined! Returning {}."
      );
      return [];
    }
    return JSON.parse(item);
  }

  // Directories
  static mkdir(path: string): number {
    const parsed = this.parse();

    if (parsed.filter((k) => k.path === path).length !== 0) {
      Logging.debug("Directory '%s' was not created: ERROR_EXISTS", path);
      return ERROR_EXISTS;
    }

    if (parsed.push({ path, type: "DIRECTORY" }) == 0) {
      Logging.debug("Directory '%s' was not created: ERROR_FAILED", path);
      return ERROR_FAILED;
    }

    this.storage.setItem("FileSystem", JSON.stringify(parsed));
    Logging.debug("Directory '%s' was created.", path);
    return ERROR_NONE;
  }

  // Files
  static write(
    path: string,
    content: Buffer | string,
    flags: WriteFlags = WriteFlags.WRITE
  ): number {
    if (flags == WriteFlags.WRITE) {
      const parsed = this.parse();
      let file_index: number;

      const filtered = parsed.filter((k: Record<string, any>, i: number) => {
        const expr = k.path === path;
        if (expr) file_index = i;
        return expr;
      });

      if (filtered.length !== 1) {
        Logging.debug("Could not write to file %s: ERROR_NOTFOUND", path);
        return ERROR_NOTFOUND;
      }

      parsed[file_index] = { path, content, type: "FILE" };
      this.storage.setItem("FileSystem", JSON.stringify(parsed));
      Logging.debug("Wrote to file %s", path);
      return ERROR_NONE;
    } else if (flags == WriteFlags.CREATE_AND_WRITE) {
      const parsed = this.parse();
      const filtered = parsed.filter(
        (k: Record<string, any>) => k.path === path
      );

      if (filtered.length !== 0) {
        Logging.debug("Could not write and create file %s: ERROR_EXISTS", path);
        return ERROR_EXISTS;
      }

      if (parsed.push({ path, content, type: "FILE" }) == 0) {
        Logging.debug("Could not write and create file %s: ERROR_FAILED", path);
        return ERROR_FAILED;
      }

      this.storage.setItem("FileSystem", JSON.stringify(parsed));
      Logging.debug("Successfully created and wrote to file %s", path);
      return ERROR_NONE;
    }
  }
}
