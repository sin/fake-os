import { GetDoc, GetScreen } from "../Canvas";
import Logging from "../Logging";

class TextConsole {
  private static _consoleElement: HTMLElement;
  private static _initialized: boolean = false;

  public static Init(): void {
    const console = GetDoc().createElement("div");
    console.style.fontFamily = "monospace";
    console.style.fontSize = "16px";
    console.style.whiteSpace = "pre-wrap";
    console.style.color = "#fff";
    console.className = "text_console";

    const consoleStyle = GetDoc().createElement("style");
    consoleStyle.innerHTML = `
    .text_console > .error {
      color: #db2e2e;
    }`;
    console.appendChild(consoleStyle);

    GetScreen().appendChild(console);
    this.consoleElement = console;
    this.initialized = true;

    Logging.info("Created text console at screen `body -> #app`");
  }

  public static write_str(message: string, type?: "error") {
    if (type !== undefined) {
      this.consoleElement.innerHTML += `<span class="${type}">${message}</span>`;
      GetScreen().scrollIntoView({ block: "end" });
      return;
    }
    this.consoleElement.innerHTML += message;
    GetScreen().scrollIntoView({ block: "end" });
  }

  public static set consoleElement(value: HTMLElement) {
    this._consoleElement = value;
  }

  public static get consoleElement(): HTMLElement {
    return this._consoleElement;
  }

  public static set initialized(value: boolean) {
    if (typeof value != "boolean") {
      throw new TypeError("Value is not a boolean");
    }

    this._initialized = value;
  }

  public static get initialized(): boolean {
    return this._initialized;
  }
}

export default TextConsole;
