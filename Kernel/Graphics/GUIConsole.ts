import { GetDoc, GetScreen } from "../Canvas";
import Logging from "../Logging";

class GUIConsole {
  private static _consoleElement: HTMLElement;
  private static _initialized: boolean = false;

  public static Init(): void {
    const console = GetDoc().createElement("div");
    console.className = "gui_console";

    GetScreen().appendChild(console);
    this.consoleElement = console;
    this.initialized = true;

    Logging.info("Created GUI console screen at body -> #app");
  }

  public static set consoleElement(value: HTMLElement) {
    this._consoleElement = value;
  }

  public static get consoleElement(): HTMLElement {
    return this._consoleElement;
  }

  public static set initialized(value: boolean) {
    if (typeof value != "boolean") {
      throw new TypeError("Value is not a boolean");
    }

    this._initialized = value;
  }

  public static get initialized(): boolean {
    return this._initialized;
  }
}

export default GUIConsole;
