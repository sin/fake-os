import InitUserspace from "../Userspace/Init";
import Logging from "./Logging";

class Userspace {
  public static Init(): void {
    Logging.info("Loading userspace...");
    InitUserspace();
  }
}

export default Userspace;
