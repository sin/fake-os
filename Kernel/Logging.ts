import TextConsole from "./Graphics/TextConsole";

class Logging {
  static #debug: boolean = false;

  static info(message: string, ...fmt: any[]): void {
    console.info(`[${__filename}] ${message}`, ...fmt);

    if (TextConsole.initialized) {
      TextConsole.write_str(` [info] ${message}\n`);
    }
  }

  static error(message: string, ...fmt: any[]): void {
    console.error(`[${__filename}] ${message}`, ...fmt);

    if (TextConsole.initialized) {
      for (const iterator of message.split("\n")) {
        TextConsole.write_str(`[error] ${iterator}\n`, "error");
      }
    }
  }

  static debug(message: string, ...fmt: any[]): void {
    console.debug(message, ...fmt);

    if (this.debug_enabled && TextConsole.initialized) {
      TextConsole.write_str(`DEBUG: ${message}\n`);
    }
  }

  static get debug_enabled(): boolean {
    return this.#debug;
  }

  static set debug_enabled(value: boolean) {
    if (typeof value != "boolean") {
      throw new TypeError("The value must be of type boolean");
    }
    this.#debug = value;
  }
}

export default Logging;
