import Canvas from "./Canvas";
import LocalStorageFileSystem, { WriteFlags } from "./FileSystem/LSFS";
import TextConsole from "./Graphics/TextConsole";
import Logging from "./Logging";
import Panic, { PanicErrorHandler } from "./Panic";
import Userspace from "./Userspace";

// FakeOS Entrypoint!
function Init() {
  Canvas.Init();
  TextConsole.Init();

  // NOTE: Logging here because the console is not initialized before these lines
  Logging.info("Initializing FakeOS...");
  // throw new Error("Yo");
  // Panic("Test panic");

  Userspace.Init();

  LocalStorageFileSystem.mkdir("/hello_world");
  LocalStorageFileSystem.write(
    "/hello_world/file.txt",
    "Hello, world!",
    WriteFlags.CREATE_AND_WRITE
  );
}

// When the window loaded successfully, run the init function
window.addEventListener("load", Init);
window.addEventListener("error", PanicErrorHandler);

// Set the global FakeOS object to control FakeOS via the console
// TODO: Implement
declare global {
  interface Window {
    FakeOS: { enable_debug?: (value: boolean) => void };
  }
}
window.FakeOS = {};

export default Init;
